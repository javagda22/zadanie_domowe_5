package com.sda.javagda22.zaddom5.zad1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        Random generator = new Random();
        for (int i = 0; i < 10; i++) {
            list.add(generator.nextInt(401) - 200); // -200 - 200
        }

        double suma = 0;
        //
        for (int i = 0; i < list.size(); i++) {
            suma = suma + list.get(i);
        }
        //
        System.out.println("Suma wynosi: " + suma);
        double srednia = suma / list.size();

        System.out.println("Srednia wynosi: " + srednia);

        // sortowanie listy
        System.out.println("Lista: " + list);

        // kopiowanie elementów do nowej listy
        List<Integer> kopia = new ArrayList<>(list);

        Collections.sort(list);
        System.out.println("Lista (po sortowaniu): " + list);

        // liczenie mediany - wartość środkowa w posortowanej kolekcji.
        if (list.size() % 2 == 0) {
            // 0 1 2 3 >|4 5|< 6 7 8 9
            int srodkowy_1 = list.get(list.size() / 2); // 5
            int srodkowy_2 = list.get((list.size() / 2) - 1); //4

            double mediana = (srodkowy_1 + srodkowy_2) / 2.0;
            System.out.println("Mediana : " + mediana);
        } else {
            // 11 elementów
            int mediana = list.get((list.size() / 2)); // 5
            System.out.println("Mediana: " + mediana);
        }

        // największy i najmniejszy
        int max = list.get(0);
        int min = list.get(0);

        System.out.println("Wypisanie testowe: " + kopia); // orginalny układ
        System.out.println("Wypisanie testowe: " + list); // posortowane
        //  1 2 3 4
        // operuję na kopii zamiast na posortowanej liście.
        for (int i = 0; i < kopia.size(); i++) {
            if (min > kopia.get(i)) {
                min = kopia.get(i);
            }
            if (max < kopia.get(i)) {
                max = kopia.get(i);
            }
        }

        int indeksMax = kopia.indexOf(max);
        int indeksMin = kopia.indexOf(min);

        System.out.println("Indeks maximum: " + indeksMax);
        System.out.println("Indeks minimum: " + indeksMin);
    }
}
