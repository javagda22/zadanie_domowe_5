package com.sda.javagda22.zaddom5.zad3;

public class Ciocia extends Osoba implements ICzlonekRodziny {
    public Ciocia(String imie) {
        super(imie);
    }

    @Override
    public boolean czyDorosly() {
        return true;
    }
}
