package com.sda.javagda22.zaddom5.zad3;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<ICzlonekRodziny> lista = new ArrayList<>();
        lista.add(new Matka("Gosia"));
        lista.add(new Ojciec("Marian"));
        lista.add(new Corka("Ela"));
        lista.add(new Syn("Alex"));
        lista.add(new Ciocia("Janka"));

        for (ICzlonekRodziny czlonekRodziny : lista) {
            czlonekRodziny.przedstawSie();
            System.out.println("^ jest dorosly = " + czlonekRodziny.czyDorosly());;
            System.out.println();
        }
    }
}
