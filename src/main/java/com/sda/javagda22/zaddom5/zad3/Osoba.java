package com.sda.javagda22.zaddom5.zad3;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class Osoba {
    protected String imie;

    public Osoba(String imie) {
        this.imie = imie;
    }
}
