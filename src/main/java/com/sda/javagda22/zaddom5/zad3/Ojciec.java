package com.sda.javagda22.zaddom5.zad3;

public class Ojciec extends Osoba implements ICzlonekRodziny{
    public Ojciec(String imie) {
        super(imie);
    }

    @Override
    public void przedstawSie() {
        System.out.println("I am Your father" + " - " + imie);
    }

    @Override
    public boolean czyDorosly() {
        return true;
    }
}
