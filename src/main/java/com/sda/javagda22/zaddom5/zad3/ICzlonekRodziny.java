package com.sda.javagda22.zaddom5.zad3;

public interface ICzlonekRodziny {
    default void przedstawSie(){
        System.out.println("I am just a simple family member");
    }

    boolean czyDorosly();
}
