package com.sda.javagda22.zaddom5.zad3;

public class Corka extends Osoba implements ICzlonekRodziny {
    public Corka(String imie) {
        super(imie);
    }

    @Override
    public void przedstawSie() {
        System.out.println("I am daughter :)" + " - " + imie);
    }

    @Override
    public boolean czyDorosly() {
        return false;
    }
}
