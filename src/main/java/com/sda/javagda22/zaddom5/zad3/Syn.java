package com.sda.javagda22.zaddom5.zad3;

public class Syn extends Osoba implements ICzlonekRodziny {

    public Syn(String imie) {
        super(imie);
    }

    @Override
    public void przedstawSie() {
        System.out.println("Who's asking?" + " - " + imie);
    }

    @Override
    public boolean czyDorosly() {
        return false;
    }

}
