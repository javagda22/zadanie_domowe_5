package com.sda.javagda22.zaddom5.zad3;

public class Matka extends Osoba implements ICzlonekRodziny {
    public Matka(String imie) {
        super(imie);
    }

    @Override
    public void przedstawSie() {
        System.out.println("I am mother" + " - " + imie);
    }

    @Override
    public boolean czyDorosly() {
        return true;
    }
}
