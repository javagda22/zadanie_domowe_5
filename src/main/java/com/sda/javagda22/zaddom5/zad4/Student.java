package com.sda.javagda22.zaddom5.zad4;

import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private String nazwisko;
    private String nrIndeksu;
    private String imie;

    private List<Double> listaOcen;

}
