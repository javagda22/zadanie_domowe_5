package com.sda.javagda22.zaddom5.zad4;

import lombok.Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Data
public class Dziennik {
    private List<Student> studentList = new ArrayList<>();

    /**
     * Metoda dodania do listy nowego studenta.
     *
     * @param st - nowy student, który ma być dodany do listy.
     */
    public void dodajStudenta(Student st) {
        studentList.add(st);
    }

    /**
     * Metoda usuwa podanego (w parametrze) studenta z listy.
     *
     * @param st - student którego chcemy usunąć z listy (dziennika).
     */
    public void usunStudenta(Student st) {
        studentList.remove(st);
    }

    public void usunStudenta(String indeksStudenta) {
        for (int i = 0; i < studentList.size(); i++) { // pętla - iterujemy wszystkich studentów
            if (studentList.get(i).getNrIndeksu().equals(indeksStudenta)) { // porównujemy nr. indeksu
                studentList.remove(i);
                // break przerwie pętle
                // po usunięciu studenta nie ma sensu dalej iterować kolekcje, ponieważ spodziewamy
                // się wyłącznie jednego studenta z podanym indeksem
                break;
            }
        }
    }

    /*
     - posiadać metodę 'zwróćStudenta(String):Student' która jako parametr
      przyjmuje numer indexu studenta, a w wyniku zwraca konkretnego studenta.
     */
    public Optional<Student> znajdzStudenta(String indeks) {
        for (int i = 0; i < studentList.size(); i++) {
            // studentList.get(i) - i'ty student
            // student = studentList.get(i)
            // student.getNrIndeksu() - String
            if (studentList.get(i).getNrIndeksu().equals(indeks)) {
                Student znaleziony = studentList.get(i);

                return Optional.of(znaleziony); // zwracamy odnalezionego studenta.
                // Optional of ^ zwróć studenta w pudełku optional
            }
        }
        //
        return Optional.empty(); //jeśli nie znajdziemy studenta, zwracamy null.
        // zwróć puste pudełko
    }

    /*
    - posiadać metodę 'podajŚredniąStudenta(String):double' która przyjmuje
      indeks studenta i zwraca średnią ocen studenta.
     */
    public double podajSredniaStudenta(String indeks) {
        Optional<Student> znaleziony = znajdzStudenta(indeks);
        if (znaleziony.isPresent()) {
            Student odnaleziony = znaleziony.get();
            List<Double> ocenyStudenta = odnaleziony.getListaOcen();

            // liczymy średnią...
            int suma = 0;
            for (int i = 0; i < ocenyStudenta.size(); i++) {
                suma += ocenyStudenta.get(i);
            }

            double srednia = suma / ocenyStudenta.size();
            return srednia;
        }

        throw new RuntimeException("Podany student nie został odnaleziony!");
    }

    public List<Student> posortujStudentówPoIndeksie() {
        // 111
        // 1111
        // 222
        List<Student> kolekcja = new ArrayList<>(studentList);
        Collections.sort(kolekcja, new StudentComparator());
        return kolekcja;
    }
}
