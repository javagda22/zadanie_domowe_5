package com.sda.javagda22.zaddom5.zad4;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        // student o nazwisku A, indeksie '132', imieniu B, lista ocen - pusta
        Student student = new Student("A", "132", "B", new ArrayList<>());

        Dziennik dziennik = new Dziennik();
        // dodanie studenta do listy
        dziennik.dodajStudenta(student);

        // wypisanie dziennika
        System.out.println(dziennik);

        // zadanie - dodaj do dziennika jeszcze 3 studentów.
        Student st1 = new Student("C", "111", "d", new ArrayList<>());
        dziennik.dodajStudenta(st1); // dodanie jawne (obiekt ma zmienną i przekazuję referencję)
        st1.setImie("Wojtek");

        dziennik.dodajStudenta(new Student("E", "1111", "F", new ArrayList<>())); // anonimowe
        dziennik.dodajStudenta(new Student("G", "222", "H", new ArrayList<>())); // anonimowe
        // wypisz dziennik (sout jak wyżej)
        System.out.println(dziennik);

        Optional<Student> szukany = dziennik.znajdzStudenta("667");
        if (szukany.isPresent()) { // sprawdzenie zawartości optionala.
            Student znaleziony = szukany.get(); // wydobycie obiektu z pudełka

            System.out.println("Szukany = " + znaleziony);
            System.out.println("Znalazłem ziomka o imieniu " + znaleziony.getImie());
        }

        try {
            double srednia = dziennik.podajSredniaStudenta("667");
            System.out.println("Srednia : " + srednia);
        } catch (ArithmeticException re) {
            System.out.println("Wystąpił wyjątek: " + re.getMessage());
        } catch (RuntimeException re) {
            System.out.println("Wystąpił wyjątek: " + re.getMessage());
        }
        System.out.println("Udało się, wypisanie działa!");

        List<Student> posortowana = dziennik.posortujStudentówPoIndeksie();
        System.out.println(posortowana);
    }
}
