package com.sda.javagda22.zaddom5.zad4;

import java.util.Comparator;

public class StudentComparator implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        // -1 - o1 większy
        // 1 - o2 większy
        // 0 - są równe
        // wersja kodu sortująca alfabetycznie
//        return o1.getNrIndeksu().compareTo(o2.getNrIndeksu());

        int indeks_o1 = Integer.parseInt(o1.getNrIndeksu());
        int indeks_o2 = Integer.parseInt(o2.getNrIndeksu());

        if (indeks_o1 > indeks_o2) {
            return -1;
        } else if (indeks_o2 > indeks_o1) {
            return 1;
        }
        return 0;
    }
}
