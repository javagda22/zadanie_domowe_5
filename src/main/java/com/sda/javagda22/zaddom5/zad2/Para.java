package com.sda.javagda22.zaddom5.zad2;

import lombok.Data;

@Data
public class Para<T> {
    private T lewy;
    private T prawy;

    public Para(T lewy, T prawy) {
        this.lewy = lewy;
        this.prawy = prawy;
    }

    public void wypiszCoMaszWSrodku() {
        System.out.println("Lewy:" + lewy + ", prawy: " + prawy);
    }

// [ P | N | P | P | N | P | P | N ]
//                             ^
//// ustal rozmiar
//// rozmiar = 3 ;
//[ N | N | N ]
//             ^
//[ P | N | P | P | N | P | P | N ]
//      ^           ^           ^
//                              ^
//int indeksWstawiania = 3;
//int indeks = 4;
    public static Para[] znajdzNiepuste(Para[] zbiorPar) {
        int iloscParNiepustych = 0;
        for (Para p : zbiorPar) {
            if (p.getLewy() != null && p.getPrawy() != null) {
                iloscParNiepustych++;
            }
        }

        // ilosc par niepustych jest mi znana
        Para[] tablicaNiepustych = new Para[iloscParNiepustych];
        int indeksWstawiania = 0;
        for (Para p : zbiorPar) {
            if (p.getLewy() != null && p.getPrawy() != null) {
                tablicaNiepustych[indeksWstawiania++] = p;
            }
        }
        return tablicaNiepustych;
    }
}
