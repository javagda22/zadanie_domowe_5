package com.sda.javagda22.zaddom5.zad2;

import com.sda.javagda22.zaddom5.zad4.Student;

public class Main {
    public static void main(String[] args) {
        Para<Student>[] pary = new Para[5];
//        pary[0] = new Para<>(new Student("Marian"), new Student("Ola"));
//        pary[1] = new Para<Student>(new Student("Jaś"), null);
//        pary[2] = new Para<Student>(new Student("Marek"), new Student("Radek"));
//        pary[3] = new Para<Student>(null, new Student("Wiola"));
//        pary[4] = new Para<Student>(new Student("Monika"), new Student("Ela"));

        for (Para<Student> para : pary) {
            para.wypiszCoMaszWSrodku();
        }

        System.out.println();
        Para<Student>[] niepuste = Para.znajdzNiepuste(pary);
        for (Para<Student> para : niepuste) {
            para.wypiszCoMaszWSrodku();
        }

        Integer liczba = null;
        Para<Integer> paraLiczb = new Para<>(5, null);

    }


}
